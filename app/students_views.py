from flask import Blueprint, request, render_template, session, redirect, flash
from .models import Student
import os  # noqa
import smtplib
from email.message import EmailMessage
import random
import imghdr

main = Blueprint('main', __name__)


def generateOtp():
    return random.randint(1111, 9999)


class SendEmail():
    def __init__(self, reciever, title, sender, body=None, html=None):
        self.reciever = reciever
        self.title = title
        self.sender = sender
        self.body = body
        self.html = html

    def send_email(self):
        msg = EmailMessage()
        msg['Subject'] = self.title
        msg['From'] = self.sender
        msg['To'] = self.reciever
        msg.set_content(str(self.body))
        html_msg_body = self.html
        if self.html is not None:
            msg.add_alternative(html_msg_body, subtype='html')
        # session['response'] = str(self.body)  # Storing otp in session

        with open('app/static/fynd.jpeg', 'rb') as attach_file:
            image_name = attach_file.name
            image_type = imghdr.what(attach_file.name)
            image_data = attach_file.read()
        msg.add_attachment(image_data, maintype='image',
                           subtype=image_type, filename=image_name)

        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
            smtp.login('resultservertest@gmail.com', "Ram@12345")
            smtp.send_message(msg)


@main.route("/", methods=['GET', 'POST'])
def home():
    if request.method == 'POST':
        rollno = request.form['rollno']
        student_data = Student.query.get(rollno)
        if student_data is None:
            flash("Please Enter Valid Roll NO", "info")
            return redirect('/')
        student_email = student_data.email
        session['email'] = (student_email, rollno)  # stored for resend otp
        generated_Otp = generateOtp()
        session['response'] = str(generated_Otp)
        sendOtp = SendEmail(student_email, str(generated_Otp),
                            'resultservertest@gmail.com',
                            session['response'])
        sendOtp.send_email()
        return render_template("validateotp.html", rollno=rollno)
    return render_template("home.html")


@main.route("/resendotp", methods=['POST'])
def resend():
    if request.method == 'POST':
        if 'email' in session:  # resend otp
            student_email = session['email'][0]  # resend otp
            re_generated_otp = generateOtp()  # resend otp
            session['response'] = str(re_generated_otp)
            resendOtp = SendEmail(student_email, str(re_generated_otp),
                                  'resultservertest@gmail.com',
                                  session['response'])
            resendOtp.send_email()
            rollno = session['email'][1]
            return render_template("validateotp.html", rollno=rollno)
        return render_template("home.html")


@main.route("/validateotp/<int:rollno>", methods=['POST'])
def validateotp(rollno):
    if request.method == 'POST':
        student_data = Student.query.get(rollno)
        student_email = student_data.email
        html_msg = render_template('resultdata.html',
                                   student_data=student_data)
        user_entered_otp = request.form['otp']
        if 'response' in session:
            generated_otp = session['response']
            session.pop('response', None)
            if generated_otp == str(user_entered_otp):
                sendEmail = SendEmail(student_email, 'Result By Akash Server',
                                      'resultservertest@gmail.com', None,
                                      html_msg)
                sendEmail.send_email()
                return redirect("/endpage")
            return render_template("home.html",
                                   msg="Otp not verified Wrong OTP!")
        return render_template("home.html",
                               msg="Session Expired (Otp Already Used)\
                               Try again !")


@main.route("/endpage", methods=['GET'])
def endpage():
    return render_template("endpage.html",
                           message="Check your Email for the result")
