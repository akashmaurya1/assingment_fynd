from flask import Blueprint, session, request, render_template, redirect, flash
from .models import Student, db
import os  # noqa
from flask_admin import Admin

main2 = Blueprint('main2', __name__)
admin = Admin()


@main2.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        if request.form.get("username") == "admin" and \
           request.form.get("password") == "Ram@12345":
            session['logged_in'] = True
            return redirect("/myadmin")
        else:
            return render_template("login.html", failed=True)
    return render_template("login.html")


@main2.route("/logout", methods=["POST"])
def logout():
    if request.method == "POST":
        session.pop('logged_in', None)
        return redirect("/login")


# -------MYAdmin----------------------------------
@main2.route("/myadmin", methods=['GET', 'POST'])
def add():
    if 'logged_in' in session:
        if request.method == 'POST':
            student_data = Student(request.form)  # data from models.py
            # This Work to handle Exception if again Same data tried to added
            student_rollno = Student.query.get(student_data.rollno)
            if student_rollno is not None:
                flash("Roll No. already Exist", "info")
                return redirect('/myadmin')
            # Else if No Exception Then Add the data
            db.session.add(student_data)
            db.session.commit()
        student_datas = Student.query.all()
        return render_template("index1.html", student_datas=student_datas)
    return redirect('/login')


@main2.route("/update/<int:rollno>", methods=['GET', 'POST'])
def update(rollno):
    if 'logged_in' in session:
        if request.method == 'POST':
            student_data = Student.query.filter_by(rollno=rollno)\
                                  .update(request.form)
            db.session.commit()
            return redirect("/myadmin")
        student_data = Student.query.filter_by(rollno=rollno).first()
        return render_template('update.html', student_data=student_data)
    return redirect('/login')


@main2.route("/delete/<int:rollno>")
def delete(rollno):
    if 'logged_in' in session:
        student_data = Student.query.filter_by(rollno=rollno).first()
        db.session.delete(student_data)
        db.session.commit()
        return redirect("/myadmin")
    return redirect('/login')
